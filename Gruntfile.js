module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        less: {
            production: {
                files: {
                  "<%= pkg.assetsPath %>css/raw/style.css" : "<%= pkg.assetsPath %>less/style.less"
                }
            }
        },
        autoprefixer: {
            options: {
                browsers: ['last 10 versions', 'Firefox > 15', 'Opera >= 12']
            },
            build: {
                '<%= pkg.assetsPath %>css/raw/style.css' : '<%= pkg.assetsPath %>css/raw/style.css'
            }
        },
        concat_css: {
            all: {
              src: ["<%= pkg.assetsPath %>css/raw/*.css"],
              dest: "<%= pkg.assetsPath %>css/style.css"
            }
        },
        cssmin: {
            target: {
                files: {
                  '<%= pkg.assetsPath %>css/style.min.css' : '<%= pkg.assetsPath %>css/style.css'
                }
            }
        },
        concat: {
            dist: {
                src: [
                    '<%= pkg.assetsPath %>js/raw/*.js'
                ],
                dest: '<%= pkg.assetsPath %>js/main.js',
                separator: ' '
            }
        },
        uglify: {
            build: {
                src: '<%= pkg.assetsPath %>js/main.js',
                dest: '<%= pkg.assetsPath %>js/main.min.js'
            }
        },
        /*imagemin DASND8UJIOPASDJI9OASPTDFNJPOIsdagnjpowetsfdauerdfsunhjiatdxzgh sTDANHJIWRFGSDhijwNGASDE9JIkmoD: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: '<%= pkg.assetsPath %>img/raw',
                    src: [ * * / *.{png,jpg,gif,svg}'],
                    dest: '<%= pkg.assetsPath %>img/'
                }]
            }
        },
        */
        
        watch: {
            styles: {
                files: ['<%= pkg.assetsPath %>less/*.less'],
                tasks: ['less', 'autoprefixer','concat_css', 'cssmin'],
                options: {
                    nospawn: true
                }
            },
            scripts: {
                files: ['<%= pkg.assetsPath %>js/raw/*.js'],
                tasks: ['concat', 'uglify'],
                options: {
                    nospawn: true
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-concat-css');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['watch']);
    grunt.registerTask('images', ['imagemin']);
    grunt.registerTask('all', ['less', 'autoprefixer', 'concat_css', 'cssmin', 'concat', 'uglify'/*, 'imagemin'*/]);
}
