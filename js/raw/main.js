$ = jQuery;

var sliderSettings = {
    arrows: true,
    prevArrow: '<span class="arrow prev-arrow"></span>',
    nextArrow: '<span class="arrow next-arrow"></span>',
    dots: true,
    slidesToShow: 1,
    fade: false
};

$(document).on('ready', ready);
$(window).on('scroll', scrollFunc);
$(window).on('resize', resize);

function ready() {

    var deadline = new Date(Date.parse(new Date()) + 15 * 24 * 60 * 60 * 1000);
    initializeClock(deadline);

    $('.reviews-slider').slick(sliderSettings);

    $('.organizators-slider').slick({
        arrows: true,
        prevArrow: '<span class="arrow prev-arrow"></span>',
        nextArrow: '<span class="arrow next-arrow"></span>',
        dots: true,
        slidesToShow: 3,
        fade: false
    });

    $('.partners-slider').slick({
        arrows: true,
        prevArrow: '<span class="arrow prev-arrow"></span>',
        nextArrow: '<span class="arrow next-arrow"></span>',
        dots: true,
        slidesToShow: 6,
        fade: false
    });
}

function scrollFunc() {}

function resize() {}

// Init popups
$('.popup').click(function() {

    var target = $(this).attr('href');
    $.magnificPopup.open({
        items: {
            src: target,
            type: 'inline'
        }
    });

    return false;
});
// End init popups

// Scroll to blocks
$('.header-menu a').click(function() {

    var block = $(this).attr('href');
    if (block == '#')
        return false;

    var path = $(block).offset().top - 79;
    $('body, html').animate({
        scrollTop: path
    }, 800);

    return false;
});
// End scroll to blocks

// Scroll to form
$('.to-form').click(function() {

    var path = $('.bottom-request-form-container').offset().top - 79;
    $('body, html').animate({
        scrollTop: path
    }, 800, function() {
        $('input[name="name"]').focus();
    });

    return false;
});

// Timer functions
function getTimeRemaining(endtime) {

    var t = Date.parse(endtime) - Date.parse(new Date());
    var seconds = Math.floor((t / 1000) % 60);
    var minutes = Math.floor((t / 1000 / 60) % 60);
    var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
    var days = Math.floor(t / (1000 * 60 * 60 * 24));
    return {
        'total': t,
        'days': days,
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds
    };
}

function initializeClock(endtime) {

    //var clock = $('.counter');
    var daysSpan = $('.counter-digits-days .counter-digits-placeholder');
    var hoursSpan = $('.counter-digits-hours .counter-digits-placeholder');
    var minutesSpan = $('.counter-digits-minutes .counter-digits-placeholder');
    //var secondsSpan = clock.querySelector('.seconds');

    function updateClock() {

        var t = getTimeRemaining(endtime);

        daysSpan.text(t.days);
        hoursSpan.text(('0' + t.hours).slice(-2));
        minutesSpan.text(('0' + t.minutes).slice(-2));
        //secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

        if ($('.counter-divider').css('visibility') == 'visible') {
            $('.counter-divider').css('visibility', 'hidden');
        } else {
            $('.counter-divider').css('visibility', 'visible');
        }

        if (t.total <= 0) {
            clearInterval(timeinterval);
        }
    }

    updateClock();
    var timeinterval = setInterval(updateClock, 1000);
}
// End timer functions

// Tabs
$(document).on('click', '.game-info-nav a:not(.current)', function() {

    var targetTab = $(this).attr('href');
    $('.game-info-nav a, .game-info-tab').removeClass('current');
    $(this).addClass('current');
    $(targetTab).addClass('current');

    return false;
});

$(document).on('click', '.game-locations-nav a:not(.current), .game-sides-nav a:not(.current)', function() {

    var targetTab = $(this).attr('href');
    $(this).closest('.game-info-tab').find('nav a, .game-info-innertab').removeClass('current');
    $(this).addClass('current');
    $(targetTab).addClass('current');

    return false;
});
// End tabs

// Modal gallery
$('#game-locations .game-info-img').each(function() {

    $(this).magnificPopup({
		delegate: 'a',
		type: 'image',
		tLoading: 'Загрузка изображения #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1]
		},
		image: {
			tError: '<a href="%url%">Изображение #%curr%</a> не загружено.'
		}
	});
});
// End modal gallery

//  Tabs
$(document).on('click', '.reviews-tabs-nav a:not(.current)', function() {

    var targetTab = $(this).attr('href');
    $('.reviews-tabs-nav a, .review-tab').removeClass('current');
    $(this).addClass('current');
    $(targetTab).addClass('current');
    $(targetTab).find('.reviews-slider').slick('unslick').slick(sliderSettings);

    return false;
});

$(document).on('click', '.faq-nav a:not(.current)', function() {

    var targetTab = $(this).attr('href');
    $('.faq-nav a, .faq-tab').removeClass('current');
    $(this).addClass('current');
    $(targetTab).addClass('current');

    return false;
});

$(document).on('click', '.prices-nav a:not(.current)', function() {

    var targetTab = $(this).attr('href');
    $('.prices-nav a, .prices-tab').removeClass('current');
    $(this).addClass('current');
    $(targetTab).addClass('current');

    return false;
});

$(document).on('click', '.bottom-tabs-tab:not(.active)', function() {

    var targetTab = $(this).attr('data-tab');
    $('.bottom-tabs-tab, .bottom-address').removeClass('active');
    $(this).addClass('active');
    $(targetTab).addClass('active');

    return false;
});
// End tabs
